# gitpod-dot-files

This repository is used with GitPod to enable code signing (among other things)

Docs: https://www.gitpod.io/docs/configure/user-settings/dotfiles

## debug.sh

This script allows debugging the dotenv file per https://www.gitpod.io/docs/configure/user-settings/dotfiles

## install.sh

This runs on gitpod startup for every pod