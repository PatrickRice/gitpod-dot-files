#!/usr/bin/env bash

sudo apt install openssl -y
mkdir -p ~/.ssh

openssl enc -d -aes-256-cbc -pbkdf2 -iter 20000 -in ./git_ssh_signing_key.encrypted -out ~/.ssh/git_ssh_signing_key -pass "pass:$GIT_SSH_SIGNING_KEY_ENCRYPTION_PASSWORD"

cp ./git_ssh_signing_key.pub ~/.ssh/git_ssh_signing_key.pub
git config --global gpg.format ssh
git config --global user.signingkey ~/.ssh/git_ssh_signing_key.pub
git config --global commit.gpgsign true
git config --global gpg.ssh.allowedSignersFile "~/.ssh/allowed_ssh_git_signers"
chmod 600 ~/.ssh/git_ssh_signing_key
